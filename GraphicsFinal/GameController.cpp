#include "GameController.h"
#include "WindowController.h"
#include "Fonts.h"
#include "ToolWindow.h"
#include <random>
#include <list>
#include <algorithm>

GameController::GameController()
{
	m_shaderColor = { };
	m_shaderDiffuse = { };
	m_camera = { };
	m_meshBoxes.clear();
}

GameController::~GameController()
{
}

void GameController::Initialize()
{
	GLFWwindow* window = WindowController::GetInstance().GetWindow();
	M_ASSERT(glewInit() == GLEW_OK, "Failed to initialize GLEW");
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glClearColor(0.1f, 0.1f, 0.1, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	srand(time(0));

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	m_camera = Camera(WindowController::GetInstance().GetResolution());
}

Mesh* GameController::CreateBox()
{
	random_device device;
	mt19937 generator(device());
	uniform_real_distribution<float> coord(-2.0f, 2.0f);

	Mesh* mesh = new Mesh();
	mesh->Create(&m_shaderDiffuse, "../Assets/Models/Box.obj");
	mesh->SetCameraPosition(m_camera.GetPosition());
	mesh->SetPosition({ coord(generator), coord(generator), coord(generator) });
	mesh->SetScale({ 0.02f, 0.02f, 0.02f });

	return mesh;
}

void GameController::ResetLightsPos()
{
	for (auto& light : Mesh::Lights)
	{
		light.SetPosition({ 0.0f, 0.0f, 0.1f });
	}
}

void GameController::ResetPotPos()
{
	for (auto* box : m_meshBoxes)
	{
		box->SetPosition({ 0.0f, 0.0f, 0.0f });
	}
}

void GameController::ResetTransform()
{
	for (auto* box : m_meshBoxes)
	{
		box->SetPosition({ 0.0f, 0.0f, 0.0f });
		box->SetRotation({ 0.0f, 0.0f, 0.0f });
		box->ResetScale();
	}

}

void GameController::RunGame()
{
	GraphicsFinal::ToolWindow^ toolWindow = gcnew GraphicsFinal::ToolWindow();
	toolWindow->Show();

	m_shaderColor = Shader();
	m_shaderColor.LoadShaders("Color.vert", "Color.frag");
	m_shaderDiffuse = Shader();
	m_shaderDiffuse.LoadShaders("Diffuse.vert", "Diffuse.frag");
	m_shaderSkybox = Shader();
	m_shaderSkybox.LoadShaders("Skybox.vert", "Skybox.frag");
	m_shaderFont = Shader();
	m_shaderFont.LoadShaders("Font.vert", "Font.frag");

	Mesh m = Mesh();
	m.Create(&m_shaderColor, "../Assets/Models/Asteroid1.ase");
	m.SetPosition({ 3.0f, 3.0f, 0.0f });
	m.SetColor({ 1.0f, 1.0f, 1.0f });
	m.SetScale({ 0.02f, 0.02f, 0.02f});
	Mesh::Lights.push_back(m);

	Mesh fighter = Mesh();
	fighter.Create(&m_shaderDiffuse, "../Assets/Models/Fighter.ase");
	fighter.SetCameraPosition(m_camera.GetPosition());
	fighter.SetScale({ 0.0015f, 0.0015f, 0.0015f });
	fighter.SetPosition({ 0.0f, 0.0f, 0.0f });
	m_meshBoxes.push_back(&fighter);

	//Mesh cube = Mesh();
	//cube.Create(&m_shaderDiffuse, "../Assets/Models/Cube.obj", 1000);
	//cube.SetCameraPosition(m_camera.GetPosition());
	//cube.SetScale({ 0.1f, 0.1f, 0.1f });
	//cube.SetPosition({ 0.25f, 0.25f, 0.25f });
	//m_meshBoxes.push_back(cube);

	//Mesh wall = Mesh();
	//wall.Create(&m_shaderDiffuse, "../Assets/Models/Wall.obj");
	//wall.SetCameraPosition(m_camera.GetPosition());
	//wall.SetScale({ 0.05f, 0.05f, 0.05f });
	//wall.SetPosition({ 0.0f, 0.0f, 0.0f });
	//m_meshBoxes.push_back(wall);

	//Skybox skybox = Skybox();
	//skybox.Create(&m_shaderSkybox, "../Assets/Models/Skybox.obj",
	//	{	"../Assets/Textures/Skybox/right.jpg",
	//		"../Assets/Textures/Skybox/left.jpg",
	//		"../Assets/Textures/Skybox/top.jpg",
	//		"../Assets/Textures/Skybox/bottom.jpg",
	//		"../Assets/Textures/Skybox/front.jpg",
	//		"../Assets/Textures/Skybox/back.jpg" });


	//Mesh plane = Mesh();
	//plane.Create(&m_shaderDiffuse, "../Assets/Models/Plane.obj");
	//plane.SetCameraPosition(m_camera.GetPosition());
	//plane.SetScale({ 0.3f, 0.3f, 0.3f });
	//plane.SetPosition({ 0.0f, 0.0f, 0.0f });
	//m_meshBoxes.push_back(plane);

	//Mesh window = Mesh();
	//window.Create(&m_shaderDiffuse, "../Assets/Models/Window.obj");
	//window.SetCameraPosition(m_camera.GetPosition());
	//window.SetScale({ 0.1f, 0.1f, 0.1f });
	//window.SetPosition({ 0.0f, 0.0f, 0.0f });
	//m_meshBoxes.push_back(window);

	Fonts f = Fonts();
	f.Create(&m_shaderFont, "arial.ttf", 100);

	double lastTime = glfwGetTime();
	int fps = 0;
	string fpsStr = "0";

	GLFWwindow* win = WindowController::GetInstance().GetWindow();
	do
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		double x = 0.0;
		double y = 0.0;
		double currentTime = glfwGetTime();

		float speedX = 1.0f;
		float speedY = 1.0f;

		fps++;
		if (currentTime - lastTime >= 1.0)
		{
			fpsStr = "FPS: " + to_string(fps);
			fps = 0;
			lastTime += 1.0f;
		}
		f.RenderText(fpsStr, 100, 80, 0.25f, { 1.0f, 1.0f, 1.0f });
		f.RenderText("Mouse Pos: " + to_string(x) + " " + to_string(y), 100, 100, 0.25f, { 1.0f, 1.0f, 1.0f });
		f.RenderText("Left Button: " + glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) ? "Down" : "Up", 100, 120, 0.25f, { 1.0f, 1.0f, 1.0f });
		f.RenderText("Middle Button: " + glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_MIDDLE) ? "Down" : "Up", 100, 140, 0.25f, { 1.0f, 1.0f, 1.0f });
		f.RenderText("Fighter Position: " + glm::to_string(fighter.GetPosition()), 100, 160, 0.25f, { 1.0f, 1.0f, 1.0f });
		f.RenderText("Fighter Rotation: " + glm::to_string(fighter.GetRotation()), 100, 180, 0.25f, { 1.0f, 1.0f, 1.0f });
		f.RenderText("Fighter Scale: " + glm::to_string(fighter.GetScale()), 100, 200, 0.25f, { 1.0f, 1.0f, 1.0f });

		glfwGetCursorPos(win, &x, &y);

		#pragma region Move Light
		if (toolWindow->moveLight)
		{
			if (glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
			{
				glm::vec3 offset = glm::vec3(0.0f);

				if (x < WindowController::GetInstance().GetResolution().m_width * 0.5f)
				{
					offset.x = -2.0f;
					speedX = (WindowController::GetInstance().GetResolution().m_width * 0.5f - x)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				else
				{
					offset.x = 2.0f;
					speedX = (x - WindowController::GetInstance().GetResolution().m_width * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				if (y < WindowController::GetInstance().GetResolution().m_height / 2.0)
				{
					offset.y = 1.0f;
					speedY = (WindowController::GetInstance().GetResolution().m_height * 0.5f - y)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}
				else
				{
					offset.y = -1.0f;
					speedY = (y - WindowController::GetInstance().GetResolution().m_height * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}

				speedX = std::clamp(speedX, 0.1f, 1.0f);
				speedY = std::clamp(speedY, 0.1f, 1.0f);

				for (auto& light : Mesh::Lights)
				{
					glm::vec3 lightPos = light.GetPosition();

					lightPos = lightPos - (lightPos - offset) * 0.01f * glm::vec3(speedX, speedY, 0.0f);

					lightPos.x = std::clamp(lightPos.x, -2.0f, 2.0f);
					lightPos.y = std::clamp(lightPos.y, -1.0f, 1.0f);

					light.SetPosition(lightPos);
				}
			}

			for (auto* box : m_meshBoxes)
			{
				box->Render(m_camera.GetProjection() * m_camera.GetView(),
					toolWindow->specST,
					glm::vec3(toolWindow->specR, toolWindow->specG, toolWindow->specB));
			}

			for (auto& light : Mesh::Lights)
			{
				light.Render(m_camera.GetProjection() * m_camera.GetView(), 4.0f, glm::vec3(3.0f, 3.0f, 3.0f));
			}
		}
		#pragma endregion

		#pragma region Color by Position
		if (toolWindow->colorByPos)
		{
			if (glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
			{
				glm::vec3 offset = glm::vec3(0.0f);

				if (x < WindowController::GetInstance().GetResolution().m_width * 0.5f)
				{
					offset.x = -2.0f;
					speedX = (WindowController::GetInstance().GetResolution().m_width * 0.5f - x)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				else
				{
					offset.x = 2.0f;
					speedX = (x - WindowController::GetInstance().GetResolution().m_width * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				if (y < WindowController::GetInstance().GetResolution().m_height / 2.0)
				{
					offset.y = 1.0f;
					speedY = (WindowController::GetInstance().GetResolution().m_height * 0.5f - y)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}
				else
				{
					offset.y = -1.0f;
					speedY = (y - WindowController::GetInstance().GetResolution().m_height * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}

				speedX = std::clamp(speedX, 0.1f, 1.0f);
				speedY = std::clamp(speedY, 0.1f, 1.0f);

				for (auto* box : m_meshBoxes)
				{
					glm::vec3 pos = box->GetPosition();

					pos = pos - (pos - offset) * 0.01f * glm::vec3(speedX, speedY, 0.0f);

					pos.x = std::clamp(pos.x, -2.0f, 2.0f);
					pos.y = std::clamp(pos.y, -1.0f, 1.0f);

					box->SetPosition(pos);
				}
			}

			for (auto* box : m_meshBoxes)
			{
				box->Render(m_camera.GetProjection() * m_camera.GetView(),
					toolWindow->specST,
					glm::vec3(toolWindow->specR, toolWindow->specG, toolWindow->specB), toolWindow->colorByPos);
			}

			for (auto& light : Mesh::Lights)
			{
				light.Render(m_camera.GetProjection() * m_camera.GetView(), 4.0f, glm::vec3(3.0f, 3.0f, 3.0f));
			}
		}
		#pragma endregion

		#pragma region Transform
		if (toolWindow->transform)
		{
			if (glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS)
			{
				glm::vec3 offset = glm::vec3(0.0f);

				if (x < WindowController::GetInstance().GetResolution().m_width * 0.5f)
				{
					offset.z = -2.0f;
					speedX = (WindowController::GetInstance().GetResolution().m_width * 0.5f - x)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				else
				{
					offset.z = 2.0f;
					speedX = (x - WindowController::GetInstance().GetResolution().m_width * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				if (y < WindowController::GetInstance().GetResolution().m_height / 2.0)
				{
					offset.y = 1.0f;
					speedY = (WindowController::GetInstance().GetResolution().m_height * 0.5f - y)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}
				else
				{
					offset.y = -1.0f;
					speedY = (y - WindowController::GetInstance().GetResolution().m_height * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}

				speedX = std::clamp(speedX, 0.1f, 1.0f);
				speedY = std::clamp(speedY, 0.1f, 1.0f);

				for (auto* box : m_meshBoxes)
				{
					if (toolWindow->translate)
					{
						glm::vec3 pos = box->GetPosition();
						pos = pos - (pos - offset) * 0.01f * glm::vec3(0.0f, speedY, speedX);
						pos.z = std::clamp(pos.z, -2.0f, 2.0f);
						pos.y = std::clamp(pos.y, -1.0f, 1.0f);
						box->SetPosition(pos);
					}
					if (toolWindow->rotate)
					{
						glm::vec3 rot = box->GetRotation();
						rot = rot - (rot - offset) * 0.01f * glm::vec3(0.0f, speedY, speedX);
						box->SetRotation(rot);
					}
					if (toolWindow->scale)
					{
						glm::vec3 sc = box->GetScale();
						sc = sc - (sc - offset) * 0.00001f * glm::vec3(0.0f, speedY, speedX);
						box->SetScale(sc);
					}
				}
			}

			if (glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
			{
				glm::vec3 offset = glm::vec3(0.0f);

				if (x < WindowController::GetInstance().GetResolution().m_width * 0.5f)
				{
					offset.x = -2.0f;
					speedX = (WindowController::GetInstance().GetResolution().m_width * 0.5f - x)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				else
				{
					offset.x = 2.0f;
					speedX = (x - WindowController::GetInstance().GetResolution().m_width * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_width * 0.5f);
				}
				if (y < WindowController::GetInstance().GetResolution().m_height / 2.0)
				{
					offset.y = 1.0f;
					speedY = (WindowController::GetInstance().GetResolution().m_height * 0.5f - y)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}
				else
				{
					offset.y = -1.0f;
					speedY = (y - WindowController::GetInstance().GetResolution().m_height * 0.5f)
						/ (WindowController::GetInstance().GetResolution().m_height * 0.5f);
				}

				speedX = std::clamp(speedX, 0.1f, 1.0f);
				speedY = std::clamp(speedY, 0.1f, 1.0f);

				for (auto* box : m_meshBoxes)
				{
					if (toolWindow->translate)
					{
						glm::vec3 pos = box->GetPosition();
						pos = pos - (pos - offset) * 0.01f * glm::vec3(speedX, speedY, 0.0f);
						pos.x = std::clamp(pos.x, -2.0f, 2.0f);
						pos.y = std::clamp(pos.y, -1.0f, 1.0f);
						box->SetPosition(pos);
					}
					if (toolWindow->rotate)
					{
						glm::vec3 rot = box->GetRotation();
						rot = rot - (rot - offset) * 0.1f * glm::vec3(speedX, speedY, 0.0f);
						box->SetRotation(rot);
					}
					if (toolWindow->scale)
					{
						glm::vec3 sc = box->GetScale();
						sc = sc - (sc - offset) * 0.00001f * glm::vec3(speedX, speedY, 0.0f);
						box->SetScale(sc);
					}
				}
			}

			for (auto* box : m_meshBoxes)
			{
				box->Render(m_camera.GetProjection() * m_camera.GetView(),
					toolWindow->specST,
					glm::vec3(toolWindow->specR, toolWindow->specG, toolWindow->specB), toolWindow->colorByPos);
			}

			for (auto& light : Mesh::Lights)
			{
				light.Render(m_camera.GetProjection() * m_camera.GetView(), 4.0f, glm::vec3(3.0f, 3.0f, 3.0f));
			}
		}
		#pragma endregion

		glfwSwapBuffers(win);
		glfwPollEvents();

	} while (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS
		&& glfwWindowShouldClose(win) == 0);

	for (auto* box : m_meshBoxes)
	{
		box->Cleanup();
	}

	for (auto& light : Mesh::Lights)
	{
		light.Cleanup();
	}
	//skybox.Cleanup();
	m_shaderDiffuse.Cleanup();
	m_shaderColor.Cleanup();
	m_shaderSkybox.Cleanup();
}