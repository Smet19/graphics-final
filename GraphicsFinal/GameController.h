#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include "StandardIncludes.h"
#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"
#include "SkyBox.h"

class GameController : public Singleton<GameController>
{
public:
	// Constructors / Destructors
	GameController();
	virtual ~GameController();

	// Methods
	void Initialize();
	void RunGame();
	void ResetLightsPos();
	void ResetPotPos();
	void ResetTransform();
	Mesh* CreateBox();

private:
	Shader m_shaderColor;
	Shader m_shaderDiffuse;
	Shader m_shaderFont;
	Shader m_shaderSkybox;
	Camera m_camera;
	vector<Mesh*> m_meshBoxes;
	Skybox m_skybox;
	GLuint m_vao;
};

#endif // GAMECONTROLLER_H

