#pragma once
#include "GameController.h"

namespace GraphicsFinal {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ToolWindow
	/// </summary>
	public ref class ToolWindow : public System::Windows::Forms::Form
	{
	private: System::Windows::Forms::TrackBar^ specStTB;
	private: System::Windows::Forms::RadioButton^ moveLightRB;
	private: System::Windows::Forms::RadioButton^ colorPosRB;
	private: System::Windows::Forms::RadioButton^ transformRB;

	private: System::Windows::Forms::Button^ resTeapotPosButton;
	private: System::Windows::Forms::Button^ resLightPosButton;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ spStLabel;
	private: System::Windows::Forms::Label^ spColRLabel;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TrackBar^ spColRTB;
	private: System::Windows::Forms::Label^ spColGLabel;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::TrackBar^ spColGTB;
	private: System::Windows::Forms::Label^ spColBLabel;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::TrackBar^ spColBTB;
	public:
		float specST;
		float specR;
		float specG;
		float specB;

		bool moveLight;
		bool colorByPos;
		bool transform;
		bool translate;
		bool rotate;
		bool scale;
		bool spaceScene;
	private: System::Windows::Forms::Button^ resTransfButton;
	private: System::Windows::Forms::CheckBox^ transCB;
	private: System::Windows::Forms::CheckBox^ rotCB;
	private: System::Windows::Forms::CheckBox^ scaleCB;
	private: System::Windows::Forms::RadioButton^ spaceSceneRB;
	public:

		ToolWindow(void)
		{
			InitializeComponent();
			
			specST = specStTB->Value;
			spStLabel->Text = System::Convert::ToString(specST);

			specR = spColRTB->Value / 100.0f;
			spColRLabel->Text = System::Convert::ToString(specR);

			specG = spColGTB->Value / 100.0f;
			spColGLabel->Text = System::Convert::ToString(specG);

			specB = spColBTB->Value / 100.0f;
			spColBLabel->Text = System::Convert::ToString(specB);

			moveLight = moveLightRB->Checked;
			colorByPos = colorPosRB->Checked;
			transform = transformRB->Checked;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ToolWindow()
		{
			if (components)
			{
				delete components;
			}
		}



	protected:

	protected:



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->specStTB = (gcnew System::Windows::Forms::TrackBar());
			this->moveLightRB = (gcnew System::Windows::Forms::RadioButton());
			this->colorPosRB = (gcnew System::Windows::Forms::RadioButton());
			this->transformRB = (gcnew System::Windows::Forms::RadioButton());
			this->resTeapotPosButton = (gcnew System::Windows::Forms::Button());
			this->resLightPosButton = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->spStLabel = (gcnew System::Windows::Forms::Label());
			this->spColRLabel = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->spColRTB = (gcnew System::Windows::Forms::TrackBar());
			this->spColGLabel = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->spColGTB = (gcnew System::Windows::Forms::TrackBar());
			this->spColBLabel = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->spColBTB = (gcnew System::Windows::Forms::TrackBar());
			this->resTransfButton = (gcnew System::Windows::Forms::Button());
			this->transCB = (gcnew System::Windows::Forms::CheckBox());
			this->rotCB = (gcnew System::Windows::Forms::CheckBox());
			this->scaleCB = (gcnew System::Windows::Forms::CheckBox());
			this->spaceSceneRB = (gcnew System::Windows::Forms::RadioButton());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->specStTB))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spColRTB))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spColGTB))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spColBTB))->BeginInit();
			this->SuspendLayout();
			// 
			// specStTB
			// 
			this->specStTB->Location = System::Drawing::Point(123, 82);
			this->specStTB->Maximum = 128;
			this->specStTB->Minimum = 1;
			this->specStTB->Name = L"specStTB";
			this->specStTB->Size = System::Drawing::Size(358, 45);
			this->specStTB->TabIndex = 0;
			this->specStTB->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->specStTB->Value = 4;
			this->specStTB->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::specStTB_ValueChanged);
			// 
			// moveLightRB
			// 
			this->moveLightRB->AutoSize = true;
			this->moveLightRB->Checked = true;
			this->moveLightRB->Location = System::Drawing::Point(12, 12);
			this->moveLightRB->Name = L"moveLightRB";
			this->moveLightRB->Size = System::Drawing::Size(78, 17);
			this->moveLightRB->TabIndex = 1;
			this->moveLightRB->TabStop = true;
			this->moveLightRB->Text = L"Move Light";
			this->moveLightRB->UseVisualStyleBackColor = true;
			this->moveLightRB->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::moveLightRB_CheckedChanged);
			// 
			// colorPosRB
			// 
			this->colorPosRB->AutoSize = true;
			this->colorPosRB->Location = System::Drawing::Point(12, 288);
			this->colorPosRB->Name = L"colorPosRB";
			this->colorPosRB->Size = System::Drawing::Size(104, 17);
			this->colorPosRB->TabIndex = 2;
			this->colorPosRB->Text = L"Color By Position";
			this->colorPosRB->UseVisualStyleBackColor = true;
			this->colorPosRB->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::colorPosRB_CheckedChanged);
			// 
			// transformRB
			// 
			this->transformRB->AutoSize = true;
			this->transformRB->Location = System::Drawing::Point(11, 357);
			this->transformRB->Name = L"transformRB";
			this->transformRB->Size = System::Drawing::Size(72, 17);
			this->transformRB->TabIndex = 3;
			this->transformRB->Text = L"Transform";
			this->transformRB->UseVisualStyleBackColor = true;
			this->transformRB->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::transformRB_CheckedChanged);
			// 
			// resTeapotPosButton
			// 
			this->resTeapotPosButton->Location = System::Drawing::Point(11, 311);
			this->resTeapotPosButton->Name = L"resTeapotPosButton";
			this->resTeapotPosButton->Size = System::Drawing::Size(133, 28);
			this->resTeapotPosButton->TabIndex = 4;
			this->resTeapotPosButton->Text = L"Reset Fighter Position";
			this->resTeapotPosButton->UseVisualStyleBackColor = true;
			this->resTeapotPosButton->Click += gcnew System::EventHandler(this, &ToolWindow::resTeapotPosButton_Click);
			// 
			// resLightPosButton
			// 
			this->resLightPosButton->Location = System::Drawing::Point(12, 35);
			this->resLightPosButton->Name = L"resLightPosButton";
			this->resLightPosButton->Size = System::Drawing::Size(133, 28);
			this->resLightPosButton->TabIndex = 5;
			this->resLightPosButton->Text = L"Reset Light Position";
			this->resLightPosButton->UseVisualStyleBackColor = true;
			this->resLightPosButton->Click += gcnew System::EventHandler(this, &ToolWindow::resLightPosButton_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(25, 96);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(92, 13);
			this->label2->TabIndex = 6;
			this->label2->Text = L"Specular Strength";
			// 
			// spStLabel
			// 
			this->spStLabel->AutoSize = true;
			this->spStLabel->Location = System::Drawing::Point(487, 96);
			this->spStLabel->Name = L"spStLabel";
			this->spStLabel->Size = System::Drawing::Size(25, 13);
			this->spStLabel->TabIndex = 7;
			this->spStLabel->Text = L"128";
			// 
			// spColRLabel
			// 
			this->spColRLabel->AutoSize = true;
			this->spColRLabel->Location = System::Drawing::Point(487, 147);
			this->spColRLabel->Name = L"spColRLabel";
			this->spColRLabel->Size = System::Drawing::Size(28, 13);
			this->spColRLabel->TabIndex = 10;
			this->spColRLabel->Text = L"3.00";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(30, 147);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(87, 13);
			this->label3->TabIndex = 9;
			this->label3->Text = L"Specular Color R";
			// 
			// spColRTB
			// 
			this->spColRTB->Location = System::Drawing::Point(123, 133);
			this->spColRTB->Maximum = 300;
			this->spColRTB->Name = L"spColRTB";
			this->spColRTB->Size = System::Drawing::Size(358, 45);
			this->spColRTB->TabIndex = 8;
			this->spColRTB->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->spColRTB->Value = 100;
			this->spColRTB->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::spColRTB_ValueChanged);
			// 
			// spColGLabel
			// 
			this->spColGLabel->AutoSize = true;
			this->spColGLabel->Location = System::Drawing::Point(487, 198);
			this->spColGLabel->Name = L"spColGLabel";
			this->spColGLabel->Size = System::Drawing::Size(28, 13);
			this->spColGLabel->TabIndex = 13;
			this->spColGLabel->Text = L"3.00";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(102, 198);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(15, 13);
			this->label4->TabIndex = 12;
			this->label4->Text = L"G";
			// 
			// spColGTB
			// 
			this->spColGTB->Location = System::Drawing::Point(123, 184);
			this->spColGTB->Maximum = 300;
			this->spColGTB->Name = L"spColGTB";
			this->spColGTB->Size = System::Drawing::Size(358, 45);
			this->spColGTB->TabIndex = 11;
			this->spColGTB->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->spColGTB->Value = 100;
			this->spColGTB->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::spColGTB_ValueChanged);
			// 
			// spColBLabel
			// 
			this->spColBLabel->AutoSize = true;
			this->spColBLabel->Location = System::Drawing::Point(487, 249);
			this->spColBLabel->Name = L"spColBLabel";
			this->spColBLabel->Size = System::Drawing::Size(28, 13);
			this->spColBLabel->TabIndex = 16;
			this->spColBLabel->Text = L"3.00";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(102, 249);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(14, 13);
			this->label5->TabIndex = 15;
			this->label5->Text = L"B";
			// 
			// spColBTB
			// 
			this->spColBTB->Location = System::Drawing::Point(123, 235);
			this->spColBTB->Maximum = 300;
			this->spColBTB->Name = L"spColBTB";
			this->spColBTB->Size = System::Drawing::Size(358, 45);
			this->spColBTB->TabIndex = 14;
			this->spColBTB->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->spColBTB->Value = 100;
			this->spColBTB->ValueChanged += gcnew System::EventHandler(this, &ToolWindow::spColBTB_ValueChanged);
			// 
			// resTransfButton
			// 
			this->resTransfButton->Location = System::Drawing::Point(11, 391);
			this->resTransfButton->Name = L"resTransfButton";
			this->resTransfButton->Size = System::Drawing::Size(133, 28);
			this->resTransfButton->TabIndex = 17;
			this->resTransfButton->Text = L"Reset Transform";
			this->resTransfButton->UseVisualStyleBackColor = true;
			this->resTransfButton->Click += gcnew System::EventHandler(this, &ToolWindow::resTransfButton_Click);
			// 
			// transCB
			// 
			this->transCB->AutoSize = true;
			this->transCB->Location = System::Drawing::Point(33, 426);
			this->transCB->Name = L"transCB";
			this->transCB->Size = System::Drawing::Size(70, 17);
			this->transCB->TabIndex = 18;
			this->transCB->Text = L"Translate";
			this->transCB->UseVisualStyleBackColor = true;
			this->transCB->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::transCB_CheckedChanged);
			// 
			// rotCB
			// 
			this->rotCB->AutoSize = true;
			this->rotCB->Location = System::Drawing::Point(33, 449);
			this->rotCB->Name = L"rotCB";
			this->rotCB->Size = System::Drawing::Size(58, 17);
			this->rotCB->TabIndex = 19;
			this->rotCB->Text = L"Rotate";
			this->rotCB->UseVisualStyleBackColor = true;
			this->rotCB->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::rotCB_CheckedChanged);
			// 
			// scaleCB
			// 
			this->scaleCB->AutoSize = true;
			this->scaleCB->Location = System::Drawing::Point(33, 472);
			this->scaleCB->Name = L"scaleCB";
			this->scaleCB->Size = System::Drawing::Size(53, 17);
			this->scaleCB->TabIndex = 20;
			this->scaleCB->Text = L"Scale";
			this->scaleCB->UseVisualStyleBackColor = true;
			this->scaleCB->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::scaleCB_CheckedChanged);
			// 
			// spaceSceneRB
			// 
			this->spaceSceneRB->AutoSize = true;
			this->spaceSceneRB->Location = System::Drawing::Point(11, 495);
			this->spaceSceneRB->Name = L"spaceSceneRB";
			this->spaceSceneRB->Size = System::Drawing::Size(90, 17);
			this->spaceSceneRB->TabIndex = 21;
			this->spaceSceneRB->Text = L"Space Scene";
			this->spaceSceneRB->UseVisualStyleBackColor = true;
			this->spaceSceneRB->CheckedChanged += gcnew System::EventHandler(this, &ToolWindow::spaceSceneRB_CheckedChanged);
			// 
			// ToolWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(529, 607);
			this->Controls->Add(this->spaceSceneRB);
			this->Controls->Add(this->scaleCB);
			this->Controls->Add(this->rotCB);
			this->Controls->Add(this->transCB);
			this->Controls->Add(this->resTransfButton);
			this->Controls->Add(this->spColBLabel);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->spColBTB);
			this->Controls->Add(this->spColGLabel);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->spColGTB);
			this->Controls->Add(this->spColRLabel);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->spColRTB);
			this->Controls->Add(this->spStLabel);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->resLightPosButton);
			this->Controls->Add(this->resTeapotPosButton);
			this->Controls->Add(this->transformRB);
			this->Controls->Add(this->colorPosRB);
			this->Controls->Add(this->moveLightRB);
			this->Controls->Add(this->specStTB);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"ToolWindow";
			this->Text = L"Tool Window";
			this->TopMost = true;
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->specStTB))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spColRTB))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spColGTB))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spColBTB))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		
	private: System::Void specStTB_ValueChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		specST = specStTB->Value;
		spStLabel->Text = System::Convert::ToString(specST);
	}

	private: System::Void spColRTB_ValueChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		specR = spColRTB->Value / 100.0f;
		spColRLabel->Text = System::Convert::ToString(specR);
	}

	private: System::Void spColGTB_ValueChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		specG = spColGTB->Value / 100.0f;
		spColGLabel->Text = System::Convert::ToString(specG);
	}

	private: System::Void spColBTB_ValueChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		specB = spColBTB->Value / 100.0f;
		spColBLabel->Text = System::Convert::ToString(specB);
	}
	private: System::Void moveLightRB_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		moveLight = moveLightRB->Checked;
	}

	private: System::Void colorPosRB_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		colorByPos = colorPosRB->Checked;
	}

	private: System::Void transformRB_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
	{
		transform = transformRB->Checked;
	}

	private: System::Void resLightPosButton_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		GameController::GetInstance().ResetLightsPos();
	}
	private: System::Void resTeapotPosButton_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		GameController::GetInstance().ResetPotPos();
	}
	private: System::Void resTransfButton_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		GameController::GetInstance().ResetTransform();
	}
	private: System::Void transCB_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		translate = transCB->Checked;
	}
	private: System::Void rotCB_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
	{
		rotate = rotCB->Checked;
	}
	private: System::Void scaleCB_CheckedChanged(System::Object^ sender, System::EventArgs^ e)
	{
		scale = scaleCB->Checked;
	}
	private: System::Void spaceSceneRB_CheckedChanged(System::Object^ sender, System::EventArgs^ e) 
	{
		spaceScene = spaceSceneRB->Checked;
	}
};
}
