#pragma once
#ifndef ASE_LOADER_H
#define ASE_LOADER_H

#include "StandardIncludes.h"
#include <math.h>

namespace asel
{
	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texCoord;
	};

	struct Material
	{
		Material()
		{
			name;
			Ns = 0.0f;
			Ni = 0.0f;
			d = 0.0f;
			illum = 0;
		}

		std::string name;		// Material Name
		glm::vec3 Ka;			// Ambient Color
		glm::vec3 Kd;			// Diffuse Color
		glm::vec3 Ks;			// Specular Color
		float Ns;				// Specular Exponent
		float Ni;				// Optical Density
		float d;				// Dissolve
		int illum;				// Illumination
		std::string map_Ka;		// Ambient Texture Map
		std::string map_Kd;		// Diffuse Texture Map
		std::string map_Ks;		// Specular Texture Map
		std::string map_Ns;		// Specular Hightlight Map
		std::string map_d;		// Alpha Texture Map
		std::string map_bump;	// Bump Map
	};

	struct Mesh
	{
		Mesh() {}
		Mesh(std::vector<Vertex>& _vertices)
		{
			vertices = _vertices;
		}

		std::vector<Vertex> vertices;
		Material material;
	};

	class Algorithm
	{
	public:
		static std::string getTag(const std::string& _string)
		{
			if (!_string.empty())
			{
				size_t start = _string.find_first_not_of(" \t");
				size_t finish = _string.find_first_of(" \t", start);

				if (start != std::string::npos && finish != std::string::npos)
				{
					return _string.substr(start, finish - start);
				}

				else if (start != std::string::npos)
				{
					return _string.substr(start);
				}
			}
			return "";
		}

		static std::string getLineNoTag(const std::string& _string)
		{
			if (!_string.empty())
			{
				size_t tag_start = _string.find_first_not_of(" \t");
				size_t space_start = _string.find_first_of(" \t", tag_start);
				size_t rest_start = _string.find_first_not_of(" \t", space_start);
				size_t rest_end = _string.find_last_not_of(" \t");

				if (rest_start != std::string::npos && rest_end != std::string::npos)
				{
					return _string.substr(rest_start, rest_end - rest_start + 1);
				}
				else if (rest_start != std::string::npos)
				{
					return _string.substr(rest_start);
				}
			}
			return "";
		}

		// Splits string into array of "words"
		static void splitStr(const std::string& _input, std::vector<std::string>& _ret, std::string _splitter)
		{
			_ret.clear();

			std::string temp;

			for (int i = 0; i < int(_input.size()); i++)
			{
				std::string test = _input.substr(i, _splitter.size());

				if (test == _splitter)
				{
					if (!temp.empty())
					{
						_ret.push_back(temp);
						temp.clear();
						i += (int)_splitter.size() - 1;
					}
					else
					{
						_ret.push_back("");
					}
				}
				else if (i + _splitter.size() >= _input.size())
				{
					temp += _input.substr(i, _splitter.size());
					_ret.push_back(temp);
					break;
				}
				else
				{
					temp += _input[i];
				}
			}
		}
	};

	class Loader
	{
	public:
		Loader() {};
		virtual ~Loader()
		{

		}

		bool LoadFile(std::string _filePath)
		{
			if (_filePath.substr(_filePath.size() - 4, 4) != ".ase")
				return false;

			std::ifstream file(_filePath);

			if (!file.is_open())
				return false;

			LoadedMeshes.clear();
			LoadedMaterials.clear();

			std::vector<glm::vec3> positions;
			std::vector<glm::vec2> texCoords;
			std::vector<glm::vec3> normals;

			std::vector<Vertex> vertices;
			std::vector<GLuint> indices;

			std::vector<std::string> MeshMatNames;

			Mesh tempMesh;

			std::string currentLine;
			while (std::getline(file, currentLine))
			{
				if (Algorithm::getTag(currentLine) == "*MATERIAL_LIST")
				{
					LoadMaterials(file);
				}

				if (Algorithm::getTag(currentLine) == "*GEOMOBJECT")
				{
					while (std::getline(file, currentLine) && currentLine != "}")
					{
						if (Algorithm::getTag(currentLine) == "*MESH")
						{
							while (std::getline(file, currentLine) && currentLine != "\t}")
							{
								if (Algorithm::getTag(currentLine) == "*MESH_VERTEX_LIST")
								{
									while (std::getline(file, currentLine) && currentLine != "\t\t}")
									{
										if (Algorithm::getTag(currentLine) == "*MESH_VERTEX")
										{
											glm::vec3 vpos;
											std::vector<std::string> mesh_vertex;
											Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), mesh_vertex, "\t");

											// mesh_vertex[0] = index; not used
											vpos.x = std::stof(mesh_vertex[1]);
											vpos.y = std::stof(mesh_vertex[3]);
											vpos.z = -1.0f * std::stof(mesh_vertex[2]);

											positions.push_back(vpos);
										}
									}
								}

								if (Algorithm::getTag(currentLine) == "*MESH_FACE_LIST")
								{
									unsigned int vert_face_idx = 0; // 3 times the ammount of faces
									while (std::getline(file, currentLine) && currentLine != "\t\t}")
									{
										if (Algorithm::getTag(currentLine) == "*MESH_FACE")
										{
											std::vector<std::string> mesh_face_temp;
											Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), mesh_face_temp, " ");// For some reason mesh_faces are not tabulated

											std::vector<std::string> mesh_face;

											for (auto str : mesh_face_temp)
											{
												if (str != "")
												{
													mesh_face.push_back(str);
												}
											}

											mesh_face_temp.clear();

											for (int i = 0; i < mesh_face.size(); i++)
											{
												string currStr = mesh_face[i];

												if (currStr == "A:" || currStr == "B:" || currStr == "C:")
												{
													unsigned int vert_idx = std::stoi(mesh_face[i + 1]);

													if (vert_face_idx >= vertices.size())
													{
														Vertex tempVert;
														tempVert.position = positions[vert_idx];

														vertices.push_back(tempVert);
													}
													else
													{
														vertices[vert_face_idx].position = positions[vert_idx];
													}
													vert_face_idx++;
												}
											}
										}
									}
								}

								if (Algorithm::getTag(currentLine) == "*MESH_TVERTLIST")
								{
									while (std::getline(file, currentLine) && currentLine != "\t\t}")
									{
										if (Algorithm::getTag(currentLine) == "*MESH_TVERT")
										{
											glm::vec2 vtex;
											std::vector<std::string> mesh_tvert;
											Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), mesh_tvert, "\t");

											vtex.x = std::stof(mesh_tvert[1]);
											vtex.y = std::stof(mesh_tvert[2]);
											//there's also Z, but it's always 0

											texCoords.push_back(vtex);											
										}
									}
								}

								if (Algorithm::getTag(currentLine) == "*MESH_TFACELIST")
								{
									unsigned int tface_idx = 0; // equals to vertice idx
									while (std::getline(file, currentLine) && currentLine != "\t\t}")
									{
										if (Algorithm::getTag(currentLine) == "*MESH_TFACE")
										{
											std::vector<std::string> tex_face;
											Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), tex_face, "\t");

											for (int i = 1; i < tex_face.size(); i++, tface_idx++) // tex_face[0] is MESH_TFACE index
											{
												string currStr = tex_face[i];
												unsigned int t_idx = std::stoi(currStr);

												if (tface_idx >= vertices.size())
												{
													Vertex tempVert;
													tempVert.texCoord = texCoords[t_idx];

													vertices.push_back(tempVert);
												}
												else
												{
													vertices[tface_idx].texCoord = texCoords[t_idx];
												}
											}
										}
									}
								}

								if (Algorithm::getTag(currentLine) == "*MESH_NORMALS")
								{
									unsigned int vnormal_idx = 0;


									while (std::getline(file, currentLine) && currentLine != "\t\t}")
									{
										if (Algorithm::getTag(currentLine) == "*MESH_FACENORMAL")
										{
											for (int vnumber = 0; vnumber < 3; vnumber++, vnormal_idx++)
											{
												std::getline(file, currentLine);
												if (Algorithm::getTag(currentLine) == "*MESH_VERTEXNORMAL")
												{
													std::vector<std::string> vertex_normal;
													Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), vertex_normal, "\t");

													//unsigned int mesh_index = std::stoi(vertex_normal[0]);	// As of right now, I don't need to store
																												// normals in vector, so I don't need index

													glm::vec3 normal;
													normal.x = std::stof(vertex_normal[1]);
													normal.y = std::stof(vertex_normal[3]);
													normal.z = -1.0f * std::stof(vertex_normal[2]);

													if (vnormal_idx >= vertices.size())
													{
														Vertex tempVert;
														tempVert.normal = normal;

														vertices.push_back(tempVert);
													}
													else
													{
														vertices[vnormal_idx].normal = normal;
													}
												}
											}
										}
									}
								}
							}
						}

						if (Algorithm::getTag(currentLine) == "*MATERIAL_REF") // Set material for mesh
						{
							unsigned int mat_idx = std::stoi(Algorithm::getLineNoTag(currentLine));

							if (!vertices.empty())
							{
								tempMesh = Mesh(vertices);
								if (!LoadedMaterials.empty())
									tempMesh.material = LoadedMaterials[mat_idx];

								LoadedMeshes.push_back(tempMesh);

								vertices.clear();
							}
						}
					}
				}
			}

			file.close();

			if (LoadedMeshes.empty() && vertices.empty())
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		void LoadMaterials(std::ifstream& file)
		{
			int material_count = 0;
			int material_index = 0;

			std::string currentLine;
			while (std::getline(file, currentLine) && currentLine != "}")
			{
				if (Algorithm::getTag(currentLine) == "*MATERIAL_COUNT")
				{
					material_count = std::stoi(Algorithm::getLineNoTag(currentLine));
				}

				if (Algorithm::getTag(currentLine) == "*MATERIAL")
				{
					int current_index = std::stoi(Algorithm::getLineNoTag(currentLine));

					Material tempMaterial = Material();

					if (current_index == material_index)
					{
						while (std::getline(file, currentLine) && currentLine != "\t}")
						{

							if (Algorithm::getTag(currentLine) == "*MATERIAL_NAME")
							{
								tempMaterial.name = Algorithm::getLineNoTag(currentLine);
							}
							// Ambient Color
							if (Algorithm::getTag(currentLine) == "*MATERIAL_AMBIENT")
							{
								std::vector<std::string> words;
								Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), words, "\t");

								if (words.size() != 3)
									continue;

								tempMaterial.Ka.x = std::stof(words[0]);
								tempMaterial.Ka.y = std::stof(words[1]);
								tempMaterial.Ka.z = std::stof(words[2]);
							}
							// Diffuse Color
							if (Algorithm::getTag(currentLine) == "*MATERIAL_DIFFUSE")
							{
								std::vector<std::string> words;
								Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), words, "\t");

								if (words.size() != 3)
									continue;

								tempMaterial.Kd.x = std::stof(words[0]);
								tempMaterial.Kd.y = std::stof(words[1]);
								tempMaterial.Kd.z = std::stof(words[2]);
							}
							// Specular Color
							if (Algorithm::getTag(currentLine) == "*MATERIAL_SPECULAR")
							{
								std::vector<std::string> words;
								Algorithm::splitStr(Algorithm::getLineNoTag(currentLine), words, "\t");

								if (words.size() != 3)
									continue;

								tempMaterial.Ks.x = std::stof(words[0]);
								tempMaterial.Ks.y = std::stof(words[1]);
								tempMaterial.Ks.z = std::stof(words[2]);
							}
							// Specular Exponent
							if (Algorithm::getTag(currentLine) == "*MATERIAL_SHINE")
							{
								tempMaterial.Ns = std::stof(Algorithm::getLineNoTag(currentLine));
							}
							// Dissolve
							if (Algorithm::getTag(currentLine) == "*MATERIAL_TRANSPARENCY")
							{
								tempMaterial.d = 1.0f - std::stof(Algorithm::getLineNoTag(currentLine));
							}
							// Diffuse Texture Map
							if (Algorithm::getTag(currentLine) == "*MAP_DIFFUSE")
							{
								while (std::getline(file, currentLine) && currentLine != "\t\t}")
								{
									if (Algorithm::getTag(currentLine) == "*BITMAP")
									{
										const size_t idx = currentLine.find_last_of("\"");

										if (std::string::npos != idx)
										{
											currentLine.erase(idx, idx + 1);
										}

										tempMaterial.map_Kd = Algorithm::getLineNoTag(currentLine);
										tempMaterial.map_Ks = tempMaterial.map_Kd;
									}
								}
							}
							// Specular Texture Map
							if (Algorithm::getTag(currentLine) == "*MAP_SPECULAR")
							{
								while (std::getline(file, currentLine) && currentLine != "\t\t}")
								{
									if (Algorithm::getTag(currentLine) == "*BITMAP")
									{
										const size_t idx = currentLine.find_last_of("\"");

										if (std::string::npos != idx)
										{
											currentLine.erase(idx, idx + 1);
										}

										tempMaterial.map_Ks = Algorithm::getLineNoTag(currentLine);
									}
								}
							}
							// Bump Map
							if (Algorithm::getTag(currentLine) == "*MAP_BUMP")
							{
								while (std::getline(file, currentLine) && currentLine != "\t\t}")
								{
									if (Algorithm::getTag(currentLine) == "*BITMAP")
									{
										const size_t idx = currentLine.find_last_of("\"");

										if (std::string::npos != idx)
										{
											currentLine.erase(idx, idx + 1);
										}

										tempMaterial.map_bump = Algorithm::getLineNoTag(currentLine);
									}
								}
							}
						}
					}

					LoadedMaterials.push_back(tempMaterial);
					material_index++;
				}
			}
		}

		std::vector<Mesh> LoadedMeshes;
		std::vector<Material> LoadedMaterials;
	};

}
#endif // !ASE_LOADER_H